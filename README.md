### このリポジトリについて

- protractorでAngularJSのサイトをe2eテストするファイルの入ったリポジトリです。
- Dockerコンテナを利用して、ヘッドレスでe2eテストを行うためのサンプルを入れています。

### ファイルを個別に指定してのテスト

% protractor test/e2e/config.js --specs test/e2e/spec/example_spec.js

### Dockerコンテナでのテスト

- Dockerホスト側に、/tmp/protractor-test というディレクトリでソースを配置
- Dockerコンテナ側には /test というディレクトリに割り当て

% docker run -it -v /tmp/protractor-test:/test -w="/test"  --rm mjvdende/node-chrome-protractor  ./test.sh
