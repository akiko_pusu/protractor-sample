#!/usr/bin/env bash

npm install
xvfb-run -e /dev/stdout --server-args="-screen 0 1360x1020x24 -ac +extension RANDR" \
protractor test/e2e/config-docker.js --specs test/e2e/spec/example_spec.js 
