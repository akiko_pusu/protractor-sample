var ScreenShotReporter = require('protractor-screenshot-reporter');
var HTMLReport = require('jasmine-xml2html-converter');

exports.config = {
    capabilities: {
        'browserName': 'chrome',
        'chromeOptions': {
            prefs: {
                'download': {
                    'prompt_for_download': false,
                    'default_directory': '/tmp/',
                    'args': ['lang=ja'],
                    prefs: {
                        intl: {
                            accept_languages: "ja"
                        },
                    },
                },
            },
        },
    },

    framework: 'jasmine',

    jasmineNodeOpts: {
        showColors: true,
        defaultTimeoutInterval: 30000
    },
    onPrepare: function () {
        // Add a screenshot reporter and store screenshots to `/tmp/screnshots`: 
        jasmine.getEnv().addReporter(new ScreenShotReporter({
            baseDirectory: 'screenshots'
        }));
        require('jasmine-reporters');
        jasmine.getEnv().addReporter(
            new jasmine.JUnitXmlReporter('xmloutput', true, true)
        );
    },
    // A callback function called once tests are finished.
    onComplete: function () {
        var path = require("path");
        var browserName, browserVersion;
        var reportPath = 'xmloutput';
        var capsPromise = browser.getCapabilities();
        capsPromise.then(function (caps) {
            browserName = caps.caps_.browserName.toLowerCase();
            browserName = browserName.replace(/ /g, "-");
            browserVersion = caps.caps_.version;
            return null;
        });

        var HTMLReport = require('jasmine-xml2html-converter');
        reportPath += browserName;

        // Call custom report for html output
        testConfig = {
            reportTitle: 'Test Execution Report',
            outputPath: 'xmloutput',
            seleniumServer: browser.seleniumAddress,
            applicationUrl: browser.baseUrl,
            testBrowser: browserName + ' ' + browserVersion
        };
        new HTMLReport().from('./xmloutput/TEST-angularjshomepage.xml', testConfig);
    }
};